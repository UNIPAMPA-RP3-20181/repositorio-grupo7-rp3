package model;

import java.util.Calendar;

/**
 *
 * @author deivid
 */
public class ApoliceSeguro {

    private static long nDaApolice;
    private Calendar dataContratacao;
    private double premio;
    private double valorSinistroMorte;
    private double valorSinistroIncapacitacao;
    private long nCartaoCredito;
    private double valorParcela;
    private int nParcelas;

    public ApoliceSeguro(Calendar dataContratacao, double premio, double valorSinistroMorte, double valorSinistroIncapacitacao, long nCartaoCredito, double valorParcela, int nParcelas) {
        this.dataContratacao = dataContratacao;
        this.premio = premio;
        this.valorSinistroMorte = valorSinistroMorte;
        this.valorSinistroIncapacitacao = valorSinistroIncapacitacao;
        this.nCartaoCredito = nCartaoCredito;
        this.valorParcela = valorParcela;
        this.nParcelas = nParcelas;
    }

    public static long getnDaApolice() {
        return nDaApolice;
    }

    public static void setnDaApolice(long nDaApolice) {
        ApoliceSeguro.nDaApolice = nDaApolice;
    }

    public Calendar getDataContratacao() {
        return dataContratacao;
    }

    public void setDataContratacao(Calendar dataContratacao) {
        this.dataContratacao = dataContratacao;
    }

    public double getPremio() {
        return premio;
    }

    public void setPremio(double premio) {
        this.premio = premio;
    }

    public double getValorSinistroMorte() {
        return valorSinistroMorte;
    }

    public void setValorSinistroMorte(double valorSinistroMorte) {
        this.valorSinistroMorte = valorSinistroMorte;
    }

    public double getValorSinistroIncapacitacao() {
        return valorSinistroIncapacitacao;
    }

    public void setValorSinistroIncapacitacao(double valorSinistroIncapacitacao) {
        this.valorSinistroIncapacitacao = valorSinistroIncapacitacao;
    }

    public long getnCartaoCredito() {
        return nCartaoCredito;
    }

    public void setnCartaoCredito(long nCartaoCredito) {
        this.nCartaoCredito = nCartaoCredito;
    }

    public double getValorParcela() {
        return valorParcela;
    }

    public void setValorParcela(double valorParcela) {
        this.valorParcela = valorParcela;
    }

    public int getnParcelas() {
        return nParcelas;
    }

    public void setnParcelas(int nParcelas) {
        this.nParcelas = nParcelas;
    }
    
    
}
