package model;

/**
 *
 * @author deivid
 */
public class Pessoa {
    private static int id;
    private String nome;
    private String endereco;
    private long telefone;
    private long cpf;
    private String email;

    public Pessoa(String nome, String endereco, long telefone, long cpf, String email) {
        this.nome = nome;
        this.endereco = endereco;
        this.telefone = telefone;
        this.cpf = cpf;
        this.email = email;
    }

    public static int getId() {
        return id;
    }

    public static void setId(int id) {
        Pessoa.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getEndereco() {
        return endereco;
    }

    public void setEndereco(String endereco) {
        this.endereco = endereco;
    }

    public long getTelefone() {
        return telefone;
    }

    public void setTelefone(long telefone) {
        this.telefone = telefone;
    }

    public long getCpf() {
        return cpf;
    }

    public void setCpf(long cpf) {
        this.cpf = cpf;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

}
