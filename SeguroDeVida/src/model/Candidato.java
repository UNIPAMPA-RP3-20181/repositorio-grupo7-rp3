package model;

import java.util.Calendar;

public class Candidato extends Pessoa {
    private String sexo;
    private long cep;
    private String uf;
    private Calendar dataNascimento;
    private String senha;

    public Candidato(String sexo, long cep, String uf, Calendar dataNascimento, String senha, String nome, String endereco, long telefone, long cpf, String email) {
        super(nome, endereco, telefone, cpf, email);
        this.sexo = sexo;
        this.cep = cep;
        this.uf = uf;
        this.dataNascimento = dataNascimento;
        this.senha = senha;
    }

    public String getSexo() {
        return sexo;
    }

    public void setSexo(String sexo) {
        this.sexo = sexo;
    }

    public long getCep() {
        return cep;
    }

    public void setCep(long cep) {
        this.cep = cep;
    }

    public String getUf() {
        return uf;
    }

    public void setUf(String uf) {
        this.uf = uf;
    }

    public Calendar getDataNascimento() {
        return dataNascimento;
    }

    public void setDataNascimento(Calendar dataNascimento) {
        this.dataNascimento = dataNascimento;
    }

    public String getSenha() {
        return senha;
    }

    public void setSenha(String senha) {
        this.senha = senha;
    }
    
}
