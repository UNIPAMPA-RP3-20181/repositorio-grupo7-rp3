package model;

import java.util.Calendar;

/**
 *
 * @author deivid
 */
public class Corretor extends Pessoa {

    private String nomeLogin;
    private String senha;
    private Calendar dataContratacao;
    private boolean ativo;

    public Corretor(String nomeLogin, String senha, Calendar dataContratacao, String nome, String endereco, long telefone, long cpf, String email) {
        super(nome, endereco, telefone, cpf, email);
        this.nomeLogin = nomeLogin;
        this.senha = senha;
        this.dataContratacao = dataContratacao;
        ativo = true;
    }

    public String getNomeLogin() {
        return nomeLogin;
    }

    public void setNomeLogin(String nomeLogin) {
        this.nomeLogin = nomeLogin;
    }

    public String getSenha() {
        return senha;
    }

    public void setSenha(String senha) {
        this.senha = senha;
    }

    public Calendar getDataContratacao() {
        return dataContratacao;
    }

    public void setDataContratacao(Calendar dataContratacao) {
        this.dataContratacao = dataContratacao;
    }

    public boolean isAtivo() {
        return ativo;
    }

    public void setAtivo(boolean ativo) {
        this.ativo = ativo;
    }

    

}
