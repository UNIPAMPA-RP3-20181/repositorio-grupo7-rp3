package controller;

import model.Candidato;
import model.Proposta;

public class ContratarSeguro {
    private Candidato candidato;
    private SolicitacaoSeguro solicitacao;
    private Proposta proposta;

    public ContratarSeguro(Candidato c, SolicitacaoSeguro solicitacao, Proposta p) {
        this.candidato = c;
        this.solicitacao = solicitacao;
        this.proposta = p;
    }
    public String retornarCandidato(){
        return this.candidato.toString();
    }
    public String visualizarSolicitacao(){
        return this.solicitacao.toString();
    }
    public void aprovarProposta(){
        this.proposta.aprovar();
    }
    public void reprovarProposta(){
        this.proposta.reprovar();
    }
}