package model;

import java.util.Calendar;

public class Segurado extends Candidato {

    private boolean vivo;
    private boolean incapacitado;

    public Segurado(String sexo, long cep, String uf, Calendar dataNascimento, String senha, String nome, String endereco, long telefone, long cpf, String email) {
        super(sexo, cep, uf, dataNascimento, senha, nome, endereco, telefone, cpf, email);
        vivo = true;
        incapacitado = false;
    }

    public boolean isVivo() {
        return vivo;
    }

    public void setVivo(boolean vivo) {
        this.vivo = vivo;
    }

    public boolean isIncapacitado() {
        return incapacitado;
    }

    public void setIncapacitado(boolean incapacitado) {
        this.incapacitado = incapacitado;
    }
    
}
