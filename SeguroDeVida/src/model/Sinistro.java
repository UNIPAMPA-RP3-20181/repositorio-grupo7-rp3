package model;

import java.util.Calendar;

/**
 *
 * @author deivid
 */
public class Sinistro {
    private String descricao;
    private String contato;
    private long cpfContato;
    private Calendar data;
    private double valor;
    private boolean autorizado;
    private boolean parecerAvaliador;
    
    public Sinistro(String descricao, String contato, long cpfContato, Calendar data, double valor) {
        this.descricao = descricao;
        this.contato = contato;
        this.cpfContato = cpfContato;
        this.data = data;
        this.valor = valor;
        autorizado = false;
        parecerAvaliador = false;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public String getContato() {
        return contato;
    }

    public void setContato(String contato) {
        this.contato = contato;
    }

    public long getCpfContato() {
        return cpfContato;
    }

    public void setCpfContato(long cpfContato) {
        this.cpfContato = cpfContato;
    }

    public boolean isParecerAvaliador() {
        return parecerAvaliador;
    }

    public void setParecerAvaliador(boolean parecerAvaliador) {
        this.parecerAvaliador = parecerAvaliador;
    }

    public Calendar getData() {
        return data;
    }

    public void setData(Calendar data) {
        this.data = data;
    }

    public double getValor() {
        return valor;
    }

    public void setValor(double valor) {
        this.valor = valor;
    }

    public boolean isAutorizado() {
        return autorizado;
    }

    public void setAutorizado(boolean autorizado) {
        this.autorizado = autorizado;
    }
    
    
    
}
