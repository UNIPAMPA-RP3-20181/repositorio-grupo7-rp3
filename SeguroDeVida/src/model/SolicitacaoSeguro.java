/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.util.Calendar;

/**
 *
 * @author Deivid
 */
public class SolicitacaoSeguro {

    private static int id;
    private Candidato candidato;
    private Calendar dataSolicitacao;
    private Double valorSolicitacao;
    private Calendar dataVisitaCandidato;
    private String motivoReprovacao;
    private int predisposicaoCancer;
    private int predisposicaoDiabetes;
    private int predisposicaoDemencia;
    private int predisposicaoCoracao;
    private int predisposicaoCerebral;
    private int predisposicaoHipertensao;
    private int predisposicaoPulmonar;
    private int predisposicaoOsteoporose;
    private int predisposicaoDegeneracao;
    private boolean fumante;
    private boolean alcoolista;
    private boolean aprovadaSolicitacao;

    public SolicitacaoSeguro(Candidato candidato, Calendar dataSolicitacao, Double valorSolicitacao, Calendar dataVisitaCandidato, String motivoReprovacao, int predisposicaoCancer, int predisposicaoDiabetes, int predisposicaoDemencia, int predisposicaoCoracao, int predisposicaoCerebral, int predisposicaoHipertensao, int predisposicaoPulmonar, int predisposicaoOsteoporose, int predisposicaoDegeneracao) {
        this.candidato = candidato;
        this.dataSolicitacao = dataSolicitacao;
        this.valorSolicitacao = valorSolicitacao;
        this.dataVisitaCandidato = dataVisitaCandidato;
        this.motivoReprovacao = motivoReprovacao;
        this.predisposicaoCancer = predisposicaoCancer;
        this.predisposicaoDiabetes = predisposicaoDiabetes;
        this.predisposicaoDemencia = predisposicaoDemencia;
        this.predisposicaoCoracao = predisposicaoCoracao;
        this.predisposicaoCerebral = predisposicaoCerebral;
        this.predisposicaoHipertensao = predisposicaoHipertensao;
        this.predisposicaoPulmonar = predisposicaoPulmonar;
        this.predisposicaoOsteoporose = predisposicaoOsteoporose;
        this.predisposicaoDegeneracao = predisposicaoDegeneracao;
        fumante = false;
        alcoolista = false;
        aprovadaSolicitacao = false;
    }

    public static int getId() {
        return id;
    }

    public static void setId(int id) {
        SolicitacaoSeguro.id = id;
    }

    public Candidato getCandidato() {
        return candidato;
    }

    public void setCandidato(Candidato candidato) {
        this.candidato = candidato;
    }

    public Calendar getDataSolicitacao() {
        return dataSolicitacao;
    }

    public void setDataSolicitacao(Calendar dataSolicitacao) {
        this.dataSolicitacao = dataSolicitacao;
    }

    public Double getValorSolicitacao() {
        return valorSolicitacao;
    }

    public void setValorSolicitacao(Double valorSolicitacao) {
        this.valorSolicitacao = valorSolicitacao;
    }

    public Calendar getDataVisitaCandidato() {
        return dataVisitaCandidato;
    }

    public void setDataVisitaCandidato(Calendar dataVisitaCandidato) {
        this.dataVisitaCandidato = dataVisitaCandidato;
    }

    public String getMotivoReprovacao() {
        return motivoReprovacao;
    }

    public void setMotivoReprovacao(String motivoReprovacao) {
        this.motivoReprovacao = motivoReprovacao;
    }

    public int getPredisposicaoCancer() {
        return predisposicaoCancer;
    }

    public void setPredisposicaoCancer(int predisposicaoCancer) {
        this.predisposicaoCancer = predisposicaoCancer;
    }

    public int getPredisposicaoDiabetes() {
        return predisposicaoDiabetes;
    }

    public void setPredisposicaoDiabetes(int predisposicaoDiabetes) {
        this.predisposicaoDiabetes = predisposicaoDiabetes;
    }

    public int getPredisposicaoDemencia() {
        return predisposicaoDemencia;
    }

    public void setPredisposicaoDemencia(int predisposicaoDemencia) {
        this.predisposicaoDemencia = predisposicaoDemencia;
    }

    public int getPredisposicaoCoracao() {
        return predisposicaoCoracao;
    }

    public void setPredisposicaoCoracao(int predisposicaoCoracao) {
        this.predisposicaoCoracao = predisposicaoCoracao;
    }

    public int getPredisposicaoCerebral() {
        return predisposicaoCerebral;
    }

    public void setPredisposicaoCerebral(int predisposicaoCerebral) {
        this.predisposicaoCerebral = predisposicaoCerebral;
    }

    public int getPredisposicaoHipertensao() {
        return predisposicaoHipertensao;
    }

    public void setPredisposicaoHipertensao(int predisposicaoHipertensao) {
        this.predisposicaoHipertensao = predisposicaoHipertensao;
    }

    public int getPredisposicaoPulmonar() {
        return predisposicaoPulmonar;
    }

    public void setPredisposicaoPulmonar(int predisposicaoPulmonar) {
        this.predisposicaoPulmonar = predisposicaoPulmonar;
    }

    public int getPredisposicaoOsteoporose() {
        return predisposicaoOsteoporose;
    }

    public void setPredisposicaoOsteoporose(int predisposicaoOsteoporose) {
        this.predisposicaoOsteoporose = predisposicaoOsteoporose;
    }

    public int getPredisposicaoDegeneracao() {
        return predisposicaoDegeneracao;
    }

    public void setPredisposicaoDegeneracao(int predisposicaoDegeneracao) {
        this.predisposicaoDegeneracao = predisposicaoDegeneracao;
    }

    public boolean isFumante() {
        return fumante;
    }

    public void setFumante(boolean fumante) {
        this.fumante = fumante;
    }

    public boolean isAlcoolista() {
        return alcoolista;
    }

    public void setAlcoolista(boolean alcoolista) {
        this.alcoolista = alcoolista;
    }

    public boolean isAprovadaSolicitacao() {
        return aprovadaSolicitacao;
    }

    public void setAprovadaSolicitacao(boolean aprovadaSolicitacao) {
        this.aprovadaSolicitacao = aprovadaSolicitacao;
    }

}
