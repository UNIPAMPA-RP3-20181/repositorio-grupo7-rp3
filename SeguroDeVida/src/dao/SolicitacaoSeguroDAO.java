package dao;

import connection.ConnectionFactory;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import model.SolicitacaoSeguro;

/**
 *
 * @author Deivid
 */
public class SolicitacaoSeguroDAO {

    private Connection connection = null;

    public SolicitacaoSeguroDAO() {
        connection = ConnectionFactory.getConnecntion();
    }

    public List<SolicitacaoSeguro> findAll() {

        String sql = "SELECT * FROM SolicitacoesDeSeguro,Candidato WHERE idCandidato = chaveCandidato";

        PreparedStatement statement = null;
        ResultSet resultset = null;

        List<SolicitacaoSeguro> solicitacoes = new ArrayList<>();

        try {
            statement = connection.prepareStatement(sql);
            resultset = statement.executeQuery();

            while (resultset.next()) {
                SolicitacaoSeguro solicitacaoSeguro = new SolicitacaoSeguro();
                solicitacaoSeguro.setDataSolicitacao(resultset.getString("dataSolicitacao"));

                solicitacaoSeguro.setCandidato(candidato);

                solicitacaoSeguro.setMotivoReprovacao(resultset.getString("nome"));

                solicitacoes.add(solicitacaoSeguro);
            }

        } catch (SQLException ex) {
            System.err.println("Erro " + ex);
        }

        return solicitacoes;
    }

}
