package model;

/**
 *
 * @author Deivid
 */
public class Beneficiario extends Pessoa {

    private static int idSegurado;
    private static int id;

    public Beneficiario(String nome, String endereco, long telefone, long cpf, String email) {
        super(nome, endereco, telefone, cpf, email);
    }

    public static int getIdSegurado() {
        return idSegurado;
    }

    public static void setIdSegurado(int idSegurado) {
        Beneficiario.idSegurado = idSegurado;
    }

    public static int getId() {
        return id;
    }

    public static void setId(int id) {
        Beneficiario.id = id;
    }
    
}
